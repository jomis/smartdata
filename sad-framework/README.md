# SDD Framework

Proof of Concept Implementation of the Smart Distributed Dataset Framework (SDD) for enabling usage-aware distributed datasets

This repository includes a sample setup to reproduce the evaluations, the sensitive information has been left empty for security reasons. Please substitute them accordingly with the necessary credentials.

Please also note that this prototype is being actively developed and subject to change.