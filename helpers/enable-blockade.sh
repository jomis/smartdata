#!/bin/bash

# Based on assumption of Ubuntu 16.04 LTS 

sudo apt-get update

sudo apt-get install python-pip

sudo pip install virtualenvwrapper

# Append to bashrc
# export WORKON_HOME=$HOME/.virtualenvs
# export PROJECT_HOME=$HOME/Devel
# source /usr/local/bin/virtualenvwrapper.sh

mkvirtualenv blockade

workon blockade

pip install blockade