require 'rubygems'

require 'sinatra'


require 'json'
require 'base64'
require 'yaml'

require 'dogapi'
require 'securerandom'


class DataService < Sinatra::Application

  def initialize
    super()
    secrets = YAML.load_file('secrets.yml')
    @datadog_api_key = secrets["datadog_api_key"]
    @datadog_application_key = secrets["datadog_application_key"]
    @uuid = SecureRandom.uuid
    @dog = Dogapi::Client.new(@datadog_api_key)
  end


  get '/' do
    "Anonymised Simulated DataService"

  end

  before do
    content_type :json
  end


  get '/mobility' do
    data = File.read("./data/json/mobility.json", :external_encoding => 'iso-8859-1',
  :internal_encoding => 'utf-8')
    data.to_json
  end

  get '/networks' do
    data = File.read("./data/json/network.json", :external_encoding => 'iso-8859-1',
  :internal_encoding => 'utf-8')
    for i in 1..3
      data_add = File.read("./data/json/network.json", :external_encoding => 'iso-8859-1',
      :internal_encoding => 'utf-8')
      data << data_add
    end
    data.to_json
  end

  get '/buildings' do
    data = File.read("./data/json/buildings.json", :external_encoding => 'iso-8859-1',
  :internal_encoding => 'utf-8')
    # Simulate actual size
    for i in 1..10
      data_add = File.read("./data/json/buildings.json", :external_encoding => 'iso-8859-1',
      :internal_encoding => 'utf-8')
      data << data_add
    end
    data.to_json
  end

  get '/blocks' do
    data = File.read("./data/json/buildings.json", :external_encoding => 'iso-8859-1',
  :internal_encoding => 'utf-8')
    data.to_json
  end


  get '/whoami' do
    "Data Service ID:#{@uuid}"
  end

end
