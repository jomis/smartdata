require 'rubygems'

require 'mongo'


class MongoDBMigrationStrategy

  @client = nil
  @database = nil
  
  attr_reader :database

  def initialize(host,database)
    @client = Mongo::Client.new([ host ], :database => database)
    @database = @client.database
  end
  
  def migrate_collection(host,database,collection)
    remote_client = Mongo::Client.new([ host ], :database => database)
    remote_database = remote_client.database
    
    begin
      @database[collection].create
    rescue Mongo::Error::OperationFailure
      puts "#{collection} already exists"
      @database[collection].drop
      @database[collection].drop
    end
    
    documents = remote_client[collection].find
    
    @database[collection].insert_many(documents)
    
    # puts @database[collection] = remote_database[collection]
    
  end
  
  
  def migrate_database(host,database)
    
    remote_client = remote_client = Mongo::Client.new([ host ], :database => database)
    remote_database = remote_client.database
    
    begin
      
      remote_database.collections.each do |collection|
        migrate_collection(host,database,collection.name)
      end
      
    rescue Mongo::Error::OperationFailure
      
      
    end
    
    
    
    
  end


  
end
# Test code
#
# test = MongoDBMigrationStrategy.new("localhost","test")
#
# puts test.database.collection_names
#
#
# # test.migrate_collection("128.130.172.194","urbem","bimbus")
# test.migrate_database("128.130.172.194","urbem")
