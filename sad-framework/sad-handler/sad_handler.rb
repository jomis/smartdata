require 'rubygems'


require 'dogapi'

require 'yaml'

require './data_exchange.rb'

require 'uri'

require 'open3'

require 'rest-client'

require 'redis'

require 'net/http'


class SadHandler

  @secrets = nil
  @dog= nil
  @proxy = nil
  @candidates = nil
  @migration_manager = nil

  def initialize()

    @secrets = YAML.load_file('secrets.yml')
    api_key = @secrets["datadog_api_key"]
    application_key = @secrets["datadog_application_key"]
    @dog = Dogapi::Client.new(api_key, application_key)
    @candidates = DataExchange.new("candidates")
    @migration_manager = ENV['migration_manager']
    @hostname = ENV['master_hostname']
    @redis = Redis.new(:host => ENV['redis_url'], :port => 6379, :db => 15)
    @host_ip = ENV['host_ip']
  end


  def start()
    # Set the topic that u want to listen to
    puts "Starting SAD Handler"
    @candidates.wait_to_receive("url",self)

  end

  def process(message)
    # rc = RailsConventionAnalyser.new
    # This can utilize different strategies much in the same way like the migrator
    # We implement one examplatory direclty here.
    puts "RECEIVED MESSAGE:#{message}"
    candidate = message.split(",")

    uri = URI.parse(candidate[0])
    # http://172.17.0.2:8080/networks,0.000589188,5712789,p238-217.vps.tuwien.ac.at
    puts uri.host



    # Trigger a migration once its is finished set the new route to the new url
    # http://localhost:9292/migrate?strategy=mongodb&destination_host=localhost&destination_database=test&source_host=128.130.172.194&source_database=urbem&collection=districts
    #docker ps --format "{{.ID}}: {{.Command}}"
    #docker ps -f "name=dataService" --format "{{.Names}}"

    collection = candidate[0].split("/").last
    request = "http://#{@migration_manager}/migrate?strategy=docker&destination_host=#{@host_ip}&destination_container_name=dataService&source_host=#{uri.host}&collection=#{collection}"
    puts "EXECUTING MIGRATION REQUEST:#{request}"
    request_uri = URI(request)
    response = Net::HTTP.get(request_uri)
    # response = RestClient.get(request)

    puts "SERVER RETURNED:#{response}"

    #Start the client

    command = "docker ps -f \"name=dataService\" --format \"{{.Names}}\""

    container_names,st = Open3.capture2(command)

    puts st
    puts "Retrieved container names: #{container_names.split("\n").inspect}"

    last_container = container_names.split("\n").sort{|a,b| a[/(\d+)/,1].to_i <=> b[/(\d+)/,1].to_i}.last

    puts "Last container would be: #{last_container}"
    last_container_id = 1
    if last_container != nil
      last_container_id = last_container[/(\d+)/,1].to_i
    end

    #a.sort { |a,b| a.to_i <=> b.to_i }

    i = last_container_id + 1

    puts "Last container would be: #{last_container} and we would start a new one with #{i}"

    command = "docker run -d -p 380#{i}:8080 -e data_service_port=8080 --name dataService#{i} eu.gcr.io/sadframework/data-service"

    puts command

    #We only start if we havent started a container already
    if @redis.get("#{uri.host}:#{uri.port}") == nil
      puts "No container running yet so we start it with #{i}"
      system(command)
    else
      puts "Already running"
      i = @redis.get("#{uri.host}:#{uri.port}").to_i
    end

    #Redis set the new url

    # Save migrated hosts as basic optimization strategy and only start them once
    @redis.set("#{uri.host}:#{uri.port}","#{i}")

    @redis.set(candidate[0],"http://#{@host_ip}:380#{i}/#{collection}")
    # Extend this for multiple updates
    @redis.set("http://#{@host_ip}:380#{i}/#{collection}","http://#{@host_ip}:380#{i}/#{collection}")

    puts "SUCESSFULLY HANDLED: #{message}"
  end

end

sadHandler = SadHandler.new()
sadHandler.start()
# sadHandler.process("http://172.17.0.2:8080/networks,0.000589188,5712789,p238-217.vps.tuwien.ac.at")
