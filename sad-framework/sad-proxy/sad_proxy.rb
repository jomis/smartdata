require 'rubygems'

require 'webrick'
require 'webrick/httpproxy'

require 'benchmark'

require 'dogapi'

require 'yaml'

require 'filesize'

require 'redis'

require './data_exchange.rb'


class WEBrick::HTTPRequest
	def update_uri(uri)
		@unparsed_uri = uri
		@request_uri = parse_uri(uri)
	end

	def set_cookie(cookie)
		@header['cookie'] = [cookie]
	end
end

class SadProxy

  @secrets = nil
  @dog= nil
  @proxy = nil

  @redirections = nil
  @redis = nil

	@request_counter = nil
	@total_requests = nil
	@filter_counter = nil

	#Google
	@host_extension = nil
	@start_time = nil

	@timing_loop = nil
	@stop_timing = nil
  def initialize()

    @secrets = YAML.load_file('secrets.yml')
    api_key = @secrets["datadog_api_key"]
    application_key = @secrets["datadog_application_key"]
    @dog = Dogapi::Client.new(api_key, application_key)
    @candidates = DataExchange.new("candidates")
    # @rerouting = DataExchange.new("rerouting")
    # @rerouting.wait_to_receive("request.redirect",self)

    @hostname = ENV['master_hostname']
    @redirections = {}

    @redis = Redis.new(:host => ENV['redis_url'], :port => 6379, :db => 15)

		@request_counter = 0
		@total_requests = 0
		@filter_counter = 0
		@host_extension = ENV['host_extension']

		@stop_timing = false
		@timing_loop = Thread.new do
	    loop do
	        Thread.stop if @stop_timing
					@dog.emit_point("request.count.persecond", @request_counter.to_i, :host => "#{@hostname}.#{@host_extension}")
					@request_counter = 0.to_i

	        sleep(1)
	    end

		end
  end

  def handle_request(request,response)

    # puts "[REQUEST] #{request}"
    # puts "[RESPONSE] #{response}"
    start = Time.now
    # print "Available methods for request\n"
    # print request.methods.join("\n") + "\n"
    # # print request.content_length
    #
    #
    #
    # print "\nAvailable methods for response\n"
    # print response.methods.join("\n") + "\n"
    puts "REQUEST: #{request}\n"
    puts "REQUEST: #{request.request_uri.to_s}\n"
    puts "Body Content Length:#{Filesize.from("#{response.body.length} B").pretty}\n"
    puts "Content Length:#{response.content_length}\n"

    duration =  Time.now - start
    puts "Request duration:#{duration}"
		@request_counter += 1.to_i
		@total_requests +=1.to_i
		@dog.emit_point("request.count.total", @total_requests, :host => "#{@hostname}.#{@host_extension}")


    filter(request,response.body.length,duration)

  end

  # Simple example filtering mechanism
  def filter (request, length, duration)
		sad_active = @redis.get("sad_active")
		# check_uri = @redis.get(request.request_uri.to_s)

		puts "Status of SAD:#{sad_active}"
		puts "Request in question: #{request.request_uri.to_s}"


		if sad_active == "true" && !@redis.exists(request.request_uri.to_s)
	    puts "Filtering because #{request.request_uri.to_s}"
			@filter_counter += 1
			# @dog.emit_point("request.count.filter", @filter_counter, :host => "#{@hostname}.#{@host_extension}")

	    if length > 5242880 || duration > 3
	      puts "Simple Length and Duration Trigger for #{request.request_uri.to_s}"
	      @candidates.send("#{request.request_uri.to_s},#{duration},#{length},#{@hostname}")
	    end
		else
			check_uri = @redis.get(request.request_uri.to_s)
		  puts "Not filtering #{request.request_uri.to_s} found something in REDIS: #{check_uri} or SAD #{sad_active}"
		end

  end


  def redirect_request(request,response)

    # updated_url = "http://www.derstandard.at/"
		puts "REDIRECT_REQUEST:#{request.inspect}\n"
		puts "REDIRECT_RESPONSE:#{response}"


    if @redis.exists(request.request_uri.to_s)
			updated_url = @redis.get(request.request_uri.to_s)
    # if request.request_uri.to_s == "http://172.17.0.2:8080/buildings"
      puts "Redirecting from #{request.request_uri.to_s} to #{updated_url}"
      request.update_uri(updated_url)
		else
			puts "No redirection found for #{request.request_uri.to_s}"
    end




  end


  def start_proxy()
    puts "Starting Proxyserver"


    @proxy = WEBrick::HTTPProxyServer.new(:Port => 8000, :ProxyContentHandler => method(:handle_request), :RequestCallback => method(:redirect_request))
    trap 'INT'  do @proxy.shutdown end
    trap 'TERM' do @proxy.shutdown end
    @proxy.start
  end


end

sadProxy = SadProxy.new()
sadProxy.start_proxy()



# # Dir["../../../lib/*.rb"].each {|file| require file }
# require '../../../lib/data_exchange.rb'
# dex = DataExchange.new('analysers')
# dex.send("hello",'artifact.uri')
