#!/usr/bin/env/ruby

class Distances

  TRAVELTIME = {
    vienna: {
      berkeley: 45,
      melbourne: 75,
      dublin: 8
    },
    berkeley: {
      melbourne: 59,
      dublin: 38
    },
    melbourne: {
      dublin: 80
    }
  }

  def self.travel_time(from,to)

    TRAVELTIME.fetch(from).fetch(to) rescue TRAVELTIME.fetch(to).fetch(from)
  end
end


servers = {
  vienna: {
    internal_ip: "10.99.0.23",
    external_ip: "128.130.172.213"
  },
  # dublin: {
  #   internal_ip: "10.99.0.34",
  #   external_ip: "128.130.172.178"
  # },
  melbourne: {
    internal_ip: "10.99.0.43",
    external_ip: "128.130.172.190"
  },
  berkeley: {
    internal_ip: "10.99.0.34",
    external_ip: "128.130.172.196"
  }
}

#alias system puts

def ssh_to (host,command)



end


servers.each do |servername, attributes|


  # Base rules
  command = "sudo tc qdisc del dev ens3 root"
  puts command
  system("ssh","ubuntu@#{attributes[:external_ip]}",command)

  command = "sudo tc filter del dev ens3 prio 1"
  puts command
  system("ssh","ubuntu@#{attributes[:external_ip]}",command)


  command = "sudo tc qdisc add dev ens3 handle 1: root htb"
  puts command
  system("ssh","ubuntu@#{attributes[:external_ip]}",command)


  command = "sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps"
  puts command
  system("ssh","ubuntu@#{attributes[:external_ip]}",command)


  # Docker0 Bridge Base Rules
  # Currently doesn work like that using puma instead

  # Base rules
  # command = "sudo tc qdisc del dev docker0 root"
  # puts command
  # system("ssh","ubuntu@#{attributes[:external_ip]}",command)
  #
  # command = "sudo tc filter del dev docker0 prio 1"
  # puts command
  # system("ssh","ubuntu@#{attributes[:external_ip]}",command)
  #
  #
  # command = "sudo tc qdisc add dev docker0 handle 1: root htb"
  # puts command
  # system("ssh","ubuntu@#{attributes[:external_ip]}",command)
  #
  #
  # command = "sudo tc class add dev docker0 parent 1: classid 1:1 htb rate 1000Mbps"
  # puts command
  # system("ssh","ubuntu@#{attributes[:external_ip]}",command)



  # For each city
  servers.keys.each_with_index do |city,index|

    next if city == servername

    command = "sudo tc class add dev ens3 parent 1: classid 1:1#{index} htb rate 100Mbps"
    puts command
    system("ssh","ubuntu@#{attributes[:external_ip]}",command)

    command = "sudo tc qdisc add dev ens3 parent 1:1#{index} handle 2#{index}: netem delay #{Distances.travel_time(servername,city)}ms #{(Distances.travel_time(servername,city)/10.0).ceil}ms distribution normal"
    puts command
    system("ssh","ubuntu@#{attributes[:external_ip]}",command)

    command = "sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst #{servers[city][:internal_ip]}/32 flowid 1:1#{index}"
    puts command
    system("ssh","ubuntu@#{attributes[:external_ip]}",command)


    # Add mobillity classes

    #EDGE
    command = "sudo tc class add dev docker0 parent 1: classid 1:31 htb rate 50kbps"
    puts command
    system("ssh","ubuntu@#{attributes[:external_ip]}",command)

    command = "sudo tc qdisc add dev docker0 parent 1:31 handle 41: netem delay 200ms 50ms distribution normal"
    puts command
    system("ssh","ubuntu@#{attributes[:external_ip]}",command)



  end



  # sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
  # sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 8ms 2ms


end


# EDGE|edge|2.5G|GPRS|gprs)
#   command=slow
#   bandwidth=50kbps
#   latency=200ms
#   ;;
# 3G|3g)
#   command=slow
#   bandwidth=1000kbps
#   latency=200ms
#   ;;
# 4G|4g)
#   command=slow
#   bandwidth=10000kbps
#   latency=100ms
#   ;;
#

#
# # Filtering host adapter via class and destination ip
#
# # Vienna <-> Dublin 8ms
# sudo tc qdisc add dev ens3 handle 1: root htb
# sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
# sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 8ms 2ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.43/32 flowid 1:11
#
# # On Dublin
# sudo tc qdisc add dev ens3 handle 1: root htb
# sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
# sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 8ms 2ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11
#
#
# # Vienna <-> Berkeley 45ms
#
# sudo tc class add dev ens3 parent 1: classid 1:12 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:12 handle 11: netem delay 45ms 4ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.34/32 flowid 1:12
#
# # On Berkeley
# sudo tc qdisc add dev ens3 handle 1: root htb
# sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
# sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:11 handle 11: netem delay 45ms 4ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11
#
#
# # Vienna <--> Melbourne 74ms
#
# sudo tc class add dev ens3 parent 1: classid 1:13 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:13 handle 12: netem delay 74ms 7ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.45/32 flowid 1:13
#
# # On Melbourne
#
# sudo tc qdisc add dev ens3 handle 1: root htb
# sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
# sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
# sudo tc qdisc add dev ens3 parent 1:11 handle 12: netem delay 74ms 7ms distribution normal
#
# sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11
#
#
#
#
# # Berkley <-> Melbourne 59 ms distance light in fiber
#
# # Berkley - Dublin 38 ms
#
# # Berkley - Vienna 45ms
#
# # Docker
# sudo iptables -A PREROUTING -t mangle -s 172.17.0.2/32 -i docker0 -j MARK --set-mark 6
# sudo iptables -A PREROUTING -t mangle -s 172.17.0.2/32 -i docker0 -j MARK --set-mark 6
# sudo tc filter add dev ens3 handle 6 fw classid 1:11
