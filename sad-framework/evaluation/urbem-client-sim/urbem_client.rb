require 'rubygems'

require 'minitest/autorun'
require 'minitest/benchmark'


require 'dogapi'


require_relative 'http.rb'

require 'json'
require 'yaml'

require 'socket'


# Client simulation for evaluation

class TestURBEM < Minitest::Unit::TestCase

  @name = "Test"
  @client_type = "Fat"
  @client_kind = "Local"
  @dog = nil
  @dogstatsd = nil
  #Google
	@host_extension = nil


  def setup
    #ENV['http_proxy'] = 'http://localhost:8000'
    #ENV['das_url'] = 'http://localhost'
    secrets = YAML.load_file('secrets.yml')
    api_key = secrets["datadog_api_key"]
    application_key = secrets["datadog_application_key"]


    @name = ENV['client_name']
    @client_type = ENV['client_type']
    @client_kind = ENV['client_kind']
    @hostname = ENV['master_hostname']
    # @host_extension = "c.sad-framework.internal"
    @host_extension = ENV['host_extension']

    # Note that submitting events does not require the application key.
    @dog = Dogapi::Client.new(api_key, application_key)
    # @dogstatsd = Datadog::Statsd.new
  end


  def test_query_buildings

    start_time = Time.now
    puts "Starting request for buildings"

    url = "#{ENV['das_url']}/buildings"
    puts url

    begin
      response = HTTP.get(url)
      puts response
      duration = Time.now - start_time
      puts "Duration buildings: #{duration}"
      # @dogstatsd.histogram("urbem.query_time_buildings", duration)
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.buildings", duration, :host => "#{@hostname}.#{@host_extension}")
      puts "Emitted point"
      # if duration <= 1
      #   @dog.emit_point("client.#{@name}.request.time.buildings.anomality", duration, :host => "#{@hostname}.#{@host_extension}")
      # end
    rescue Net::ReadTimeout
      puts "READ TIMEOUT"
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.buildings.error", 1, :host => "#{@hostname}.#{@host_extension}")
    end

  end

  def test_query_blocks

    start_time = Time.now
    puts "Starting request for buildings blocks"

    url = "#{ENV['das_url']}/blocks"
    puts url

    begin
      response = HTTP.get(url)
      puts response
      duration = Time.now - start_time
      puts "Duration blocks: #{duration}"
      # @dogstatsd.histogram("urbem.query_time_buildings", duration)
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.blocks", duration, :host => "#{@hostname}.#{@host_extension}")
      puts "Emitted point"
      # if duration <= 1
      #   @dog.emit_point("client.#{@name}.request.time.blocks.anomality", duration, :host => "#{@hostname}.#{@host_extension}")
      # end
    rescue Net::ReadTimeout
      puts "READ TIMEOUT"
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.blocks.error", 1, :host => "#{@hostname}.#{@host_extension}")
    end

  end


  def test_query_network

    start_time = Time.now
    puts "Starting request networks"

    url = "#{ENV['das_url']}/networks"
    puts url

    begin
      response = HTTP.get(url)
      puts response
      duration = Time.now - start_time
      puts "Duration networks: #{duration}"
      # @dogstatsd.histogram("urbem.query_time_buildings", duration)
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.networks", duration, :host => "#{@hostname}.#{@host_extension}")
      puts "Emitted point"
      # if duration <= 1
      #   @dog.emit_point("client.#{@name}.request.time.networks.anomality", duration, :host => "#{@hostname}.#{@host_extension}")
      # end
    rescue Net::ReadTimeout
      puts "READ TIMEOUT"
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.networks.error", 1, :host => "#{@hostname}.#{@host_extension}")
    end


  end


  def test_query_mobility
    start_time = Time.now
    puts "Starting request mobility"

    url = "#{ENV['das_url']}/mobility"
    puts url

    begin
      response = HTTP.get(url)
      puts response
      duration = Time.now - start_time
      puts "Duration mobility: #{duration}"
      # @dogstatsd.histogram("urbem.query_time_buildings", duration)
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.mobility", duration, :host => "#{@hostname}.#{@host_extension}")
      puts "Emitted point"
    rescue Net::ReadTimeout
      puts "READ TIMEOUT"
      @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.mobility.error", 1, :host => "#{@hostname}.#{@host_extension}")
    end

  end


end
