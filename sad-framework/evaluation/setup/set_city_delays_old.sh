#!/bin/bash


# Filtering host adapter via class and destination ip

# Vienna <-> Dublin 8ms
sudo tc qdisc add dev ens3 handle 1: root htb
sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 8ms 2ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.43/32 flowid 1:11

# On Dublin
sudo tc qdisc add dev ens3 handle 1: root htb
sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 8ms 2ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11


# Vienna <-> Berkeley 45ms

sudo tc class add dev ens3 parent 1: classid 1:12 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:12 handle 11: netem delay 45ms 4ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.34/32 flowid 1:12

# On Berkeley
sudo tc qdisc add dev ens3 handle 1: root htb
sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:11 handle 11: netem delay 45ms 4ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11


# Vienna <--> Melbourne 74ms

sudo tc class add dev ens3 parent 1: classid 1:13 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:13 handle 12: netem delay 74ms 7ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.45/32 flowid 1:13

# On Melbourne 

sudo tc qdisc add dev ens3 handle 1: root htb
sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:11 handle 12: netem delay 74ms 7ms distribution normal

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 10.99.0.40/32 flowid 1:11


# Berkley <-> Melbourne 59 ms distance light in fiber

# Berkley - Dublin 38 ms

# Berkley - Vienna 45ms 

# Docker
sudo iptables -A PREROUTING -t mangle -s 172.17.0.3/32 -i docker0 -j MARK --set-mark 6
sudo tc filter add dev ens3 handle 6 fw classid 1:11