# Run the container

docker run -d -e http_proxy=http://172.17.0.4:8000 -e das_url=http://url:port -e client_name -e client_type -e client_kind -t eu.gcr.io/sadframework/urbem-client 

# Get ip adress of container

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id
