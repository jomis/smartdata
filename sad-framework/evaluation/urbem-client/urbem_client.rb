require 'rubygems'

require 'minitest/autorun'
require 'minitest/benchmark'


require 'dogapi'


require_relative 'http.rb'

require 'json'
require 'yaml'

require 'socket'


# Client simulation for evaluation

class TestURBEM < Minitest::Unit::TestCase

  @name = "Test"
  @client_type = "Fat"
  @client_kind = "Local"
  @dog = nil
  @dogstatsd = nil


  def setup
    #ENV['http_proxy'] = 'http://localhost:8000'
    #ENV['das_url'] = 'http://localhost'
    secrets = YAML.load_file('secrets.yml')
    api_key = secrets["datadog_api_key"]
    application_key = secrets["datadog_application_key"]


    @name = ENV['client_name']
    @client_type = ENV['client_type']
    @client_kind = ENV['client_kind']
    @hostname = ENV['master_hostname']

    # Note that submitting events does not require the application key.
    @dog = Dogapi::Client.new(api_key, application_key)
    # @dogstatsd = Datadog::Statsd.new
  end

  # def test_request
 #    # start = Time.now
 #    #
 #    # response = HTTP.get('http://www.jetblue.com')
 #    # puts response
 #    #
 #    # puts response.content_length
 #    #
 #    # duration =  Time.now - start
 #    # puts duration
 #
 #  end

  def query_resource_page_buildings(page)

    url = "#{ENV['das_url']}/data/urbem_data_sad/buildings?count&page=#{page}&pagesize=1000"

    response = HTTP.get(url)

    puts url
    return  JSON.parse(response.body,:symbolize_names => true)

    # return parsed_response[:_embedded]
  end

  def query_resource_page_networks(page)

    url = "#{ENV['das_url']}/data/urbem_data_sad/districtheatinglinesbackward?count&page=#{page}&pagesize=1000"

    response = HTTP.get(url)

    puts url
    return  JSON.parse(response.body,:symbolize_names => true)

    # return parsed_response[:_embedded]
  end


  def test_query_buildings

    start_time = Time.now
    puts "Starting request for buildings"

    url = "#{ENV['das_url']}/data/urbem_data_sad/buildings?count&pagesize=1000"
    puts url

    response = HTTP.get("#{ENV['das_url']}/data/urbem_data_sad/buildings?count&pagesize=1000")

    parsed_response = JSON.parse(response.body,:symbolize_names => true)

    pages = parsed_response[:_total_pages]

    buildings = []

    buildings << parsed_response[:_embedded]

    for i in 2..50#2..pages
      buildings << query_resource_page_buildings(i)
    end

    puts "Collected buildings:#{buildings.count}"
    puts "Total pages #{pages}"

    duration = Time.now - start_time


    # @dogstatsd.histogram("urbem.query_time_buildings", duration)
    @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.buildings", duration, :host => "#{@hostname}.novalocal")

  end

  def test_query_network

    start_time = Time.now
    puts "Starting request networks"

    url = "#{ENV['das_url']}/data/urbem_data_sad/districtheatinglinesbackward?count&pagesize=1000"
    puts url

    response = HTTP.get("#{ENV['das_url']}/data/urbem_data_sad/districtheatinglinesbackward?count&pagesize=1000")

    parsed_response = JSON.parse(response.body,:symbolize_names => true)

    pages = parsed_response[:_total_pages]

    networks = []

    networks << parsed_response[:_embedded]

    for i in 2..pages
      networks << query_resource_page_networks(i)
    end

    puts "Collected networks:#{networks.count}"
    puts "Total pages #{pages}"

    duration = Time.now - start_time


    # @dogstatsd.histogram("client.#{@client_type}.query.time.networks", duration)
    @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.networks", duration, :host => "#{@hostname}.novalocal")

  end


  def test_query_mobility
    start_time = Time.now
    puts "Starting request mobility"


    response = HTTP.get("#{ENV['das_url']}/data/urbem_data_sad/subway?count&pagesize=1000")

    subways = JSON.parse(response.body,:symbolize_names => true)

    response = HTTP.get("#{ENV['das_url']}/data/urbem_data_sad/bimbus?count&pagesize=1000")

    bimbus = JSON.parse(response.body,:symbolize_names => true)



    puts "Total pages #{bimbus[:_total_pages]} / #{subways[:_total_pages]}"


    duration = Time.now - start_time


    # @dogstatsd.histogram("client.#{@client_type}.query.time.mobility", duration)
    @dog.emit_point("client.#{@client_type}.#{@client_kind}.request.time.mobility", duration, :host => "#{@hostname}.novalocal")
  end


end
