require 'rubygems'

require 'json'


Dir["./json/*"].each do |file|

  puts file

  data = []
  File.open(file).each do |line|
    data << JSON.parse(line)
  end

  File.open("./json_formatted/#{File.basename(file)}","w") do |f|

    f.write(data.to_json)

  end
end
