#!/usr/bin/env/ruby
require 'rubygems'

require 'thor'

require 'json'

require 'erb'

require 'net/ssh'

require 'open3'

require 'yaml'

require 'redis'

class Distances

  TRAVELTIME = {
    vienna: {
      berkeley: 45,
      melbourne: 75,
      dublin: 8
    },
    berkeley: {
      melbourne: 59,
      dublin: 38
    },
    melbourne: {
      dublin: 80
    }
  }

  def self.travel_time(from,to)

    TRAVELTIME.fetch(from).fetch(to) rescue TRAVELTIME.fetch(to).fetch(from)
  end
end

#
# class Configurations
#
#   include ERB::Util
#   attr_accessor :mongo_url
#
#   def initialize(linkname)
#     @mongo_url = linkname
#     @template = ERB.new File.read("./restheart.yml.erb"), nil, "%<>"
#   end
#
#   def render()
#     Encoding.default_external = "UTF-8"
#     ERB.new(@template).result(binding)
#   end
#
#   def save(file)
#      File.open(file, "w+") do |f|
#        f.write(render)
#      end
#   end
#
#   def self.generate_rest_heart_config(linkname)
#
#     Encoding.default_external = "UTF-8"
#
#     template = ERB.new File.read("./restheart.yml.erb"), nil, "%<>"
#
#     mongo_url = "mongodb://#{linkname}"
#
#     renderer = ERB.new(template)
#
#     puts output = template.result
#
#     File.write("./restheart_#{linkname}.yml",output)
#
#   end
#
# end

class SetupInterface < Thor
  #DSG
  @@username = "ubuntu"
  @@host_extension = "novalocal"
  @@servers = {
    vienna: {
      internal_ip: "10.99.0.23",
      external_ip: "128.130.172.213"
    },
    # dublin: {
    #   internal_ip: "10.99.0.72",
    #   external_ip: "128.130.172.178"
    # },
    melbourne: {
      internal_ip: "10.99.0.43",
      external_ip: "128.130.172.190"
    }
    #,
    # berkeley: {
    #   internal_ip: "10.99.0.74",
    #   external_ip: "128.130.172.196"
    # }
  }
  # Google Cloud
  # @@username = "jomis"
  # @@host_extension = "c.sad-framework.internal"
  # @@servers = {
  #   vienna: {
  #     internal_ip: "10.132.0.2",
  #     external_ip: "104.199.44.11"
  #   },
  #   hongkong: {
  #     internal_ip: "10.140.0.2",
  #     external_ip: "107.167.181.152"
  #   },
  #   berkeley: {
  #     internal_ip: "10.128.0.2",
  #     external_ip: "104.155.161.107"
  #   }
  # }

  desc "start NAME(ALL)", "Start Sad Framework on NAME or ALL"
  def startSADFramework(name)
    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s
        puts "Executing for #{servername}"
        @redis = Redis.new(:host => attributes[:external_ip], :port => 6379, :db => 15)
        current_state = @redis.get("sad_active")
        puts current_state
        @redis.set("sad_active","true")
      end

    end

  end

  desc "stop  NAME(ALL)", "Stop Sad Framework on NAME or ALL"
  def stopSADFramework(name)
    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s
        puts "Executing for #{servername}"
        @redis = Redis.new(:host => attributes[:external_ip], :port => 6379, :db => 15)
        current_state = @redis.get("sad_active")
        puts current_state
        @redis.set("sad_active","false")
      end

    end

  end


  desc "cleanInstances NAME(ALL) from everything", "Stop all running containers and remove them, also removes yml files"
  def cleanInstancesTotal(name)
    @@servers.each do |servername, attributes|
      puts "Executing for #{servername}"
      if name == "ALL" || name == servername.to_s
        #Stop them

        command = "docker stop $(docker ps -q)"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


        #Remove them
        command = "docker rm $(docker ps -a -q)"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        # Remove all generated yml files
        command = "rm -rf restheart*.yml"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)
      end

    end

  end

  desc "cleanSpecific SERVICE(urbem,data,sad) from NAME(ALL)", "Stop all running containers SAD and remove them, also removes yml files"
  def cleanSpecific(service,name)
    @@servers.each do |servername, attributes|
      puts "Executing for #{servername}"
      if name == "ALL" || name == servername.to_s
        #Stop them

        command = "docker stop $(docker ps -q -f \"name=#{service}\")"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


        #Remove them
        command = "docker rm $(docker ps -a -q -f \"name=#{service}\")"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

      end

    end

  end

  desc "populateData NAME(ALL)","Populate Data on Servers"
  def populateData(name)



    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s
        puts "Starting for #{servername}"

        puts "Copying data"
        # Start MongoDBS
        command = "scp -r /Users/jomis/Documents/src/university/prototypes/smartdata/sad-framework/evaluation/data-service/dump #{@username}@#{attributes[:external_ip]}:./dump "

        puts command
        system(command)
      end

    end
  end


  desc "updateImages NAME(ALL)","Updates all images"
  def updateImages(name)


    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s

        puts "Executing for #{servername}"
        #
        # puts "Pulling Dataservice images"
        # # Start MongoDBS
        # # command = "docker pull mongo:3.2"
        # command = "docker pull mongo:3.4"
        #
        # puts command
        # system("ssh","#{@@username}@#{attributes[:external_ip]}",command)
        #
        # command = "docker pull softinstigate/restheart:2.0.1"
        #
        # puts command
        # system("ssh","#{@@username}@#{attributes[:external_ip]}",command)
        #
        #
        # command = "gcloud docker pull eu.gcr.io/sadframework/urbem-client"
        #
        # puts command
        # system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull eu.gcr.io/sadframework/sad-proxy"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull eu.gcr.io/sadframework/sad-migrator"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull eu.gcr.io/sadframework/sad-handler"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull rabbitmq:3-management"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull redis"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull eu.gcr.io/sadframework/data-service"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "gcloud docker pull eu.gcr.io/sadframework/urbem-client-sim"

        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

      end

    end


  end


  desc "populateSAD NAME(ALL)", "Start basic SAD Framework Services"
  def populateSAD(name)
    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s

        puts "Executing for #{servername}"

        puts "Executing for #{servername}"
        # First we start a proxy on each servername

        command = "docker run --name sad-redis -p 6379:6379 -d redis"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "docker run -d --hostname sad-rabbit --name sad-rabbit -p 8001:15672 -p 5671:5671 -p 5672:5672 rabbitmq:3-management"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


        command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-redis"

        redis_ip,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts redis_ip
        redis_ip = redis_ip.strip

        command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-rabbit"

        rabbit_ip,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts rabbit_ip
        rabbit_ip = rabbit_ip.strip

        # docker run --name sad-redis -p 6379:6379 -d redis
        #
        #
        #
        # docker run -d --hostname sad-rabbit --name sad-rabbit -p 8001:15672 -p 5671:5671 -p 5672:5672 rabbitmq:3-management

        # Wait till rabbit mq is here

        # puts "Waiting for rabbit mq to start"
        sleep(30)

        command = "docker run -p 9000:8000 -d -e mq_url=#{rabbit_ip} -e master_hostname=$(hostname) -e host_extension=#{@@host_extension} -e redis_url=#{redis_ip} --name sad-proxy -t eu.gcr.io/sadframework/sad-proxy"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        # Determine its ip adress

        command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-proxy"

        ip,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts ip
        ip = ip.strip

        # Start migration manager and handler

        command = "docker run -p 9293:9293 -e migration_manager_port=9293 -e master_hostname=$(hostname) -e host_extension=#{@@host_extension} -d --name sad-migrator eu.gcr.io/sadframework/sad-migrator"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-migrator"

        migration_ip,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts migration_ip
        migration_ip = migration_ip.strip


        # -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker:ro
        command = "docker run -e mq_url=#{rabbit_ip} -e migration_manager=#{migration_ip}:9293 -e master_hostname=$(hostname) -e host_ip=$(hostname -i) -e redis_url=#{redis_ip} --name sad-handler -v /var/run/docker.sock:/var/run/docker.sock -v /usr/bin/docker:/usr/bin/docker:ro -d -t eu.gcr.io/sadframework/sad-handler"
        puts command
        system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


      end

    end


  end


  desc "populateClientServices NUMBER  NAME(ALL)  DESTINATION","Start NUMBER Clients on all instances or on NAME server and connect them to DESTINATION"
  def populateClientServices(numberOfClientServices,name,destination)
    clientServiceMap = {}
    puts "Starting for #{name}"
    puts "Connecting to #{destination}"

    @@servers.each do |servername, attributes|

      if name == "ALL" || name == servername.to_s

        puts "Executing for #{servername}"

        command = "docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-proxy"

        ip,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts ip
        ip = ip.strip

        #Determine last running client

        command = "docker ps -f \"name=urbem-client\" --format \"{{.Names}}\""
        puts command

        container_names,st = Open3.capture2("ssh","#{@@username}@#{attributes[:external_ip]}",command)

        puts st
        puts "Retrieved container names: #{container_names.split("\n").inspect}"

        last_container = container_names.split("\n").sort{|a,b| a[/(\d+)/,1].to_i <=> b[/(\d+)/,1].to_i}.last

        puts "Last container would be: #{last_container}"
        last_container_id = 0
        if last_container != nil
          last_container_id = last_container[/(\d+)/,1].to_i
        end

        #a.sort { |a,b| a.to_i <=> b.to_i }

        # starting_range = last_container_id + 1

        puts "Last container would be: #{last_container} and we start a new one with #{last_container_id}"



        # if destination == "ALL"
        #       #TODO Collect them from the other servers if all
        # end
        filename = "./endpoints/dataServices_#{destination.strip}.json"
        destination_ip = @@servers[destination.strip.to_sym][:internal_ip]
        dataServices = YAML.load_file(filename)
        puts dataServices.inspect

        for i in 1..numberOfClientServices.to_i
          puts "Starting ClientServices"
          # Start Client

          # Sampling approach
          # sample = dataServices.keys.sample
          # Linear approach
          sample = dataServices.keys[i-1]
          servicePort = dataServices[sample]
          #command = "docker run -d -e http_proxy=http://#{ip.strip}:8000 -e das_url=http://#{destination_ip}:#{servicePort} -e client_name=urbem-client-#{i} -e client_type=fat -e client_kind=remote -e master_hostname=$(hostname) --name urbem-client-#{i} -t eu.gcr.io/sadframework/urbem-client"
          command = "docker run -d -e http_proxy=http://#{ip.strip}:8000 -e das_url=http://#{destination_ip}:#{servicePort} -e client_name=urbem-client-#{i.to_i+last_container_id.to_i} -e client_type=fat -e client_kind=remote -e master_hostname=$(hostname) -e host_extension=#{@@host_extension} --name urbem-client-#{i.to_i+last_container_id.to_i} -t eu.gcr.io/sadframework/urbem-client-sim"
          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)
          #
          # puts command
          # system("ssh","ubuntu@#{attributes[:external_ip]}",command)

        end
      #   Net::SSH.start("#{attributes[:external_ip]}", 'ubuntu') do |ssh|
      #     # capture all stderr and stdout output from a remote process
      #     output = ssh.exec!("hostname")
      #     puts output
      #
      #     for i in 1..numberOfClientServices.to_i
      #
      #     end
      #
      #
      #
      #   end
      #
      #
      # end


      end

    end


  end


  desc "populateDatabases NUMBER  NAME(ALL)","Start NUMBER Databases on all instances or on NAME server)"
  def populateDatabases(numberOfDataServices,name)


    dataServiceMap = {}
    clientMap = {}

    puts "Starting for #{name}"

    @@servers.each do |servername, attributes|


      if name == "ALL" || name == servername.to_s



        puts "Executing for #{servername}"
        for i in 1..numberOfDataServices.to_i
          puts "Starting MongoDB"
          # Start MongoDBS
          # command = "docker run -d -p 370#{i}:27017 -v /home/#{@@username}/dump:/dump --name mongodb#{i} mongo:3.2"
          command = "docker run -d -p 370#{i}:27017 -v /home/#{@@username}/dump:/dump --name mongodb#{i} mongo:3.4"


          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

          puts "Importing Data"

          command = "docker exec mongodb#{i} mongorestore --db urbem_data_sad --collection buildings /dump/urbem_data_sad/buildings.bson"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

          command = "docker exec mongodb#{i} mongorestore --db urbem_data_sad --collection districtheatinglinesbackward /dump/urbem_data_sad/districtheatinglinesbackward.bson"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


          command = "docker exec mongodb#{i} mongorestore --db urbem_data_sad --collection bimbus /dump/urbem_data_sad/bimbus.bson"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

          command = "docker exec mongodb#{i} mongorestore --db urbem_data_sad --collection subway /dump/urbem_data_sad/subway.bson"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)


          dataServiceMap["mongodb#{i}"] = "370#{i}"

          File.open("./endpoints/mongoDatabases_#{servername}.json","w") do |f|
            f.write(dataServiceMap.to_json)
          end


        end
      end

    end


  end

  desc "populateDataServices NUMBER  NAME(ALL)","Start NUMBER Dataservices on all instances or on NAME server. MODE (ADD,START)"
  def populateDataServices(numberOfDataServices,name)


    dataServiceMap = {}
    clientMap = {}

    puts "Starting for #{name}"

    @@servers.each do |servername, attributes|


      if name == "ALL" || name == servername.to_s


        # startNumber = 1
        # if mode == "ADD"
        #   puts "Adding #{numberOfDataServices} on top"
        #   startNumber = from
        #   numberOfDataServices=numberOfDataServices.to_i+startNumber.to_i
        # else
        #   startNumber = 1
        # end
        # puts "Executing for #{servername} with #{startNumber} and #{numberOfDataServices}"
        j = 1
        for i in 1..numberOfDataServices.to_i

          puts "Starting Custom Restheart Interface"

          # Generate custom configs REPLACE_ME

          # text = File.read("./restheart.yml.template")
          # new_contents = text.gsub(/REPLACE_ME/, "mongodb#{i}")
          #
          #
          # # To write changes to the file, use:
          # File.open("./restheart_#{i}.yml", "w") {|file| file.puts new_contents }
          #
          # # Copy the file to the server

          system("scp","./restheart_#{j}.yml","#{@@username}@#{attributes[:external_ip]}:restheart_#{i}.yml")

          puts "Config file transferred to server"




          command = "docker run -d -p 380#{i}:8080 -v $PWD/restheart_#{i}.yml:/opt/restheart/etc/restheart.yml:ro --name dataService#{i} --link mongodb#{j} softinstigate/restheart:2.0.1"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

          puts "Restheart started"

          # if i%10 == 0
          #   j+= 1
          # end

          j=1


          # dataServiceMap["mongodb#{i}"] = "370#{i}"
          clientMap["dataService#{i}"] = "380#{i}"

          # File.open("./endpoints/mongoDatabases_#{servername}.json","w") do |f|
          #   f.write(dataServiceMap.to_json)
          # end
          #
          File.open("./endpoints/dataServices_#{servername}.json","w") do |f|
            f.write(clientMap.to_json)
          end



        end
      end

    end


  end

  desc "populateDataSimServices NUMBER  NAME(ALL)","Start NUMBER Datasimservices on all instances or on NAME server."
  def populateDataSimServices(numberOfDataServices,name)


    dataServiceMap = {}
    clientMap = {}

    puts "Starting for #{name}"

    @@servers.each do |servername, attributes|


      if name == "ALL" || name == servername.to_s


        # startNumber = 1
        # if mode == "ADD"
        #   puts "Adding #{numberOfDataServices} on top"
        #   startNumber = from
        #   numberOfDataServices=numberOfDataServices.to_i+startNumber.to_i
        # else
        #   startNumber = 1
        # end
        # puts "Executing for #{servername} with #{startNumber} and #{numberOfDataServices}"
        for i in 1..numberOfDataServices.to_i

          puts "Starting Data Sim Services"

          #docker run -p 3801:8080 -e data_service_port=8080 -d --name dataService1 eu.gcr.io/sadframework/data-service

          command = "docker run -d -p 380#{i}:8080 -e data_service_port=8080 --name dataService#{i} eu.gcr.io/sadframework/data-service"

          puts command
          system("ssh","#{@@username}@#{attributes[:external_ip]}",command)

          puts "Service  started"


          # dataServiceMap["mongodb#{i}"] = "370#{i}"
          clientMap["dataService#{i}"] = "380#{i}"

          # File.open("./endpoints/mongoDatabases_#{servername}.json","w") do |f|
          #   f.write(dataServiceMap.to_json)
          # end
          #
          File.open("./endpoints/dataServices_#{servername}.json","w") do |f|
            f.write(clientMap.to_json)
          end



        end
      end

    end


  end




end


SetupInterface.start(ARGV)
