require 'rubygems'

require 'webrick'
require 'webrick/httpproxy'

proxy = WEBrick::HTTPProxyServer.new Port: 8000 :RequestCallback => Proc.new{|req,res| puts req.request_line, req.raw_header}

trap 'INT'  do proxy.shutdown end
trap 'TERM' do proxy.shutdown end

proxy.start