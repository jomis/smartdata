require 'rubygems'

require 'sinatra'

# Load available strategies
Dir["./strategies/*.rb"].each {|file| require file }


require 'json'
require 'yaml'

require 'dogapi'
require 'securerandom'



class MigrationManager < Sinatra::Application

  def initialize
    super()
    secrets = YAML.load_file('secrets.yml')
    @datadog_api_key = secrets["datadog_api_key"]
    @datadog_application_key = secrets["datadog_application_key"]
    @uuid = SecureRandom.uuid
    @dog = Dogapi::Client.new(@datadog_api_key)
    @hostname = ENV['master_hostname']
    #Google
  	@host_extension = ENV['host_extension']
  end

  get '/' do
    "SAD Framework Migration Manager"
  end

  get '/whoami' do
    "SAD Migration Manager ID:#{@uuid}"

  end


  get '/migrate' do
    puts "Got Migration request with #{params.inspect}"
    start = Time.now
    return_message = {}
    # data = JSON.parse request.body.read
    if params.has_key?('strategy')
      migration_strategy = params['strategy']
      if migration_strategy == "mongodb"

          destination_host = params['destination_host']
          destination_database = params['destination_database']

          source_host = params['source_host']
          source_database = params['source_database']

          mongodb_strategy = MongoDBMigrationStrategy.new(destination_host,destination_database)

          if params.has_key?('collection')
            collection = params['collection']
            mongodb_strategy.migrate_collection(source_host,source_database,collection)
          else
            mongodb_strategy.migrate_database(source_host,source_database)
          end
      end

      if migration_strategy == "docker"

          puts "Starting DOCKER migrationdoc"

          destination_host = params['destination_host']
          destination_container_name = params['destination_container_name']

          source_host = params['source_host']

          docker_strategy = DockerMigrationStrategy.new(destination_host,destination_container_name)

          if params.has_key?('collection')
            collection = params['collection']
            docker_strategy.migrate_collection(source_host,collection)
            # @dog.emit_point("migrator.#{migration_strategy}.size", filesize, :host => "#{@hostname}.#{@host_extension}")
          else
            docker_strategy.migrate_all(source_host)
          end
      end

      duration = Time.now - start

      @dog.emit_point("migrator.#{migration_strategy}.migrationtime", duration, :host => "#{@hostname}.#{@host_extension}")
      "Migrated successfully in #{duration} for #{migration_strategy}"
    else
      "Please provide strategy source and destination parameters"

    end


  end

  # post '/tu' do
  #     data = JSON.parse request.body.read
  #     puts "Received #{data}"
  #     @technical_unit_store.store(data['name'],data)
  # end
  #
  # get '/tu' do
  #   return_message = {}
  #   if params.has_key?('name')
  #     return_message=@technical_unit_store.retrieve(params['name'])
  #   end
  #
  #   if params.has_key?('search')
  #     return_message=@technical_unit_store.search(params['search'])
  #   end
  #
  #   if params.empty?
  #     return_message=@technical_unit_store.get_all_keys()
  #   end
  #   return_message.to_json
  # end
  #
  # post '/is' do
  #     data = JSON.parse request.body.read
  #     puts "Received #{data}"
  #     @infrastructure_specification_store.store(data['name'],data)
  # end
  #
  #
  # get '/is' do
  #   return_message = {}
  #   if params.has_key?('name')
  #
  #     puts "Retrieving #{params['name']}"
  #     found = @infrastructure_specification_store.retrieve(params['name'])
  #     puts "Found #{found}"
  #     return_message = found
  #   end
  #
  #   if params.has_key?('search')
  #     return_message = @infrastructure_specification_store.search(params['search'])
  #   end
  #
  #   if params.empty?
  #     return_message=@infrastructure_specification_store.get_all_keys()
  #   end
  #
  #   return_message.to_json
  #
  # end
  #
  # post '/du' do
  #     data = JSON.parse request.body.read
  #     puts "Received #{data}"
  #     @deployment_unit_store.store(data['name'],data)
  # end
  #
  #
  #
  # get '/du' do
  #   return_message = {}
  #   if params.has_key?('name')
  #     return_message=@deployment_unit_store.retrieve(params['name'])
  #   end
  #
  #   if params.has_key?('search')
  #     return_message=@deployment_unit_store.search(params['search'])
  #   end
  #
  #   if params.empty?
  #     return_message=@deployment_unit_store.get_all_keys()
  #   end
  #   return_message.to_json
  # end
  #
  # post '/di' do
  #     data = JSON.parse request.body.read
  #     puts "Received #{data}"
  #     @deployment_instance_store.store(data['name'],data)
  # end
  #
  #
  #
  # get '/di' do
  #   return_message = {}
  #   if params.has_key?('name')
  #     return_message=@deployment_instance_store.retrieve(params['name'])
  #   end
  #
  #   if params.has_key?('search')
  #     return_message=@deployment_instance_store.search(params['search'])
  #   end
  #
  #   if params.empty?
  #     return_message=@deployment_instance_store.get_all_keys()
  #   end
  #   return_message.to_json
  #
  # end
  #
  #

end
