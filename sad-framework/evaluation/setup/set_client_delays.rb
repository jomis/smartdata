#!/usr/bin/env/ruby

require 'open3'
require 'toxiproxy'

class ClientTypes

  BITRATE = {
    edge: {
      rate: 45
    },
    mobile: {
      rate: 59
    },
    fat: {
      rate: 80
    }
  }

  def self.travel_time(client_type)

    BITRATE.fetch(client_type)
  end
end


servers = {
  vienna: {
    internal_ip: "10.99.0.40",
    external_ip: "128.130.172.213"
  },
  dublin: {
    internal_ip: "10.99.0.43",
    external_ip: "128.130.172.178"
  },
  melbourne: {
    internal_ip: "10.99.0.45",
    external_ip: "128.130.172.190"
  },
  berkeley: {
    internal_ip: "10.99.0.34",
    external_ip: "128.130.172.196"
  }
}

#Toxiproxy approach

# Toxiproxy.host = 'http://localhost:8474'
#
# Toxiproxy.populate([{
#   name: "mysql_master",
#   listen: "localhost:21212",
#   upstream: "localhost:3306",
# },{
#   name: "mysql_read_only",
#   listen: "localhost:21213",
#   upstream: "localhost:3306",
# }])


# command = "docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba \
#          pumba netem --interface eth0 --duration 1m delay --time 3000 inspiring_cray"
#
# #--cap-add=NET_ADMIN
#
#
filter = "docker ps --filter \"label=mobile=3g\" --format \"{{.Names}}\""

# command = "sudo tc filter del dev ens3 prio 1"
# puts command
# system("ssh","ubuntu@#{attributes[:external_ip]}",command)

output,st = Open3.capture2(filter)

names = output.split("\n")

puts names.inspect

servers.each do |servername, attributes|



end



# Other approaches

#
#
# command = "docker run -it --rm -v /var/run/docker.sock:/var/run/docker.sock gaiaadm/pumba \
#          pumba netem --interface eth0 --duration 1m delay --time 3000 #{output}"
#
# system(command)


# command = "docker exec -d nervous_davinci ./slow.sh 4G"

# EDGE|edge|2.5G|GPRS|gprs)
#   command=slow
#   bandwidth=50kbps
#   latency=200ms
#   ;;
# 3G|3g)
#   command=slow
#   bandwidth=1000kbps
#   latency=200ms
#   ;;
# 4G|4g)
#   command=slow
#   bandwidth=10000kbps
#   latency=100ms
#   ;;
#

#
# # Docker
# sudo iptables -A PREROUTING -t mangle -s 172.17.0.3/32 -i docker0 -j MARK --set-mark 6
# sudo tc filter add dev ens3 handle 6 fw classid 1:11
