#!/bin/bash

docker build -t eu.gcr.io/sadframework/data-service .
gcloud docker push eu.gcr.io/sadframework/data-service
