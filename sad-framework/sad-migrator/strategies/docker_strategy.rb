require 'rubygems'

require 'securerandom'

class DockerMigrationStrategy

  @host = nil
  @container_name= nil
  @username = nil

  attr_reader :database

  def initialize(host,container_name)
    @host = host
    @container_name = container_name
    @migration_folder = SecureRandom.uuid
    # @username = "ubuntu"
    @username = "jomis"
  end

  def start_new_container(destination_host)

  end

  def migrate_collection(source_host,collection)


    command = "mkdir /usr/src/app/data/#{@migration_folder}"
    puts command
    system(command)

    command = "scp -i /usr/src/app/key/dsg-cloud.pem -oStrictHostKeyChecking=no #{@username}@#{source_host}:/home/#{@username}/migration/data/#{collection}.json /usr/src/app/data/#{@migration_folder}"

    # command = "scp #{source_host}/home/ubuntu/migration/data/#{collection}.json #{@host}:/home/ubuntu/migration/data"
    puts command
    system(command)

    # filesize = File.size("/usr/src/app/data/#{@migration_folder}/#{collection}.json")
    # puts filesize
    # return filesize


  end

  def migrate_all(source_host)


    command = "mkdir /usr/src/app/data/#{@migration_folder}"
    puts command
    system(command)

    command = "scp -i /usr/src/app/key/dsg-cloud.pem -oStrictHostKeyChecking=no #{@username}@#{source_host}:/home/#{@username}/migration/data/*.json /usr/src/app/data/##{@migration_folder}"

    # command = "scp #{source_host}/home/ubuntu/migration/data/#{collection}.json #{@host}:/home/ubuntu/migration/data"
    puts command
    system(command)

  end


end
# Test code

# test = DockerMigrationStrategy.new("localhost","test")
#
# test.migrate_collection("128.130.172.213","buildings")


# test.migrate_collection("128.130.172.194","urbem","bimbus")
# test.migrate_database("128.130.172.194","urbem")
