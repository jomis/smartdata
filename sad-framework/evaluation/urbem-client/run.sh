#!/bin/bash

while true; do
	bundle exec ruby urbem_client.rb
  sleep $[ ( $RANDOM % 20 )  + 1 ]s
done



#
# if bundle exec rails runner 'puts ActiveRecord::SchemaMigration.count'; then
#   # DB already set up, migrate
#   bundle exec rake db:migrate
# else
#   # DB empty/doesn't exist yet
#   bundle exec rake db:setup
#
#   # Case for seeding ?
# fi
#
# bundle exec puma -C config/puma.rb

# Trapping stuff

# function finish {
#     # re-start service
#     sudo service mongdb start
# }
# trap finish EXIT