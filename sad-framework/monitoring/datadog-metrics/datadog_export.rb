require 'rubygems'

require 'dogapi'

require 'yaml'

secrets = YAML.load_file('secrets.yml') 

api_key = secrets["datadog_api_key"]
application_key = secrets["datadog_application_key"]

dog = Dogapi::Client.new(api_key, application_key)

# Get points from the last hour
from = Time.now - 3600
to = Time.now
# query = 'system.cpu.idle{*}by{host}'
query = 'migrator.mongodb.migrationtime{*}by{host}'
puts dog.get_points(query, from, to)