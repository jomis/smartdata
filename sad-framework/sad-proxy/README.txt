docker run -p 9000:8000 -d -t redis_url hostname !!! eu.gcr.io/sadframework/sad-proxy

#Rabbit MQ Setup
docker run -d --hostname my-rabbit --name some-rabbit rabbitmq:3

#Rabbit MQ with Management
docker run -d --hostname sad-rabbit --name sad-rabbit -p 8080:15672 rabbitmq:3-management
docker run -d --hostname sad-rabbit --name sad-rabbit -p 8001:15672 -p 5671:5671 -p 5672:5672 rabbitmq:3-management
