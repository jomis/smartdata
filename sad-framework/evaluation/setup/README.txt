# Based on assumption of Ubuntu 16.04 LTS

sudo apt-get update

sudo apt-get install unattended-upgrades

sudo dpkg-reconfigure --priority=low unattended-upgrades

# Dockerize

sudo apt-get update

sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list

sudo apt-get update

apt-cache policy docker-engine

sudo apt-get install -y docker-engine

sudo systemctl status docker

# Add the current user to the docker group

sudo usermod -aG docker $(whoami)

sudo reboot

#!/bin/bash

# Create an environment variable for the correct distribution
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"

# Add the Cloud SDK distribution URI as a package source
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee /etc/apt/sources.list.d/google-cloud-sdk.list

# Import the Google Cloud public key
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

# Update and install the Cloud SDK
sudo apt-get update && sudo apt-get install google-cloud-sdk

# Run gcloud init to get started
gcloud init

# Datadog

DD_API_KEY=2d770e67de03c8bf499b496e64ddbb4d bash -c "$(curl -L https://raw.githubusercontent.com/DataDog/dd-agent/master/packaging/datadog-agent/source/install_agent.sh)"

sudo usermod -a -G docker dd-agent

Create docker_daemon.yaml by copying the example file in the agent conf.d directory. If you have a standard install of Docker on your host, there shouldn’t be anything you need to change to get the integration to work.

/etc/dd-agent/conf.d$ sudo cp docker_daemon.yaml.example docker_daemon.yaml

sudo /etc/init.d/datadog-agent restart

check with

sudo /etc/init.d/datadog-agent info


To enable other integrations, use docker ps to identify the ports used by the corresponding applications.


# Fix Swap Issue

sudo ln -s /dev/null /etc/udev/rules.d/40-vm-hotadd.rules
sudo reboot

https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1518457/comments/140
