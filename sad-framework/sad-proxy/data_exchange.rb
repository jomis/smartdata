require 'rubygems'

require 'bunny'

class DataExchange

  def initialize(topic)

    host = ENV['mq_url']
    # endpoint = endpoint.split(":")
  #   host = endpoint[0]
  #   port = endpoint[1]

    @connection = Bunny.new(:host => host)
    @connection.start
    @channel = @connection.create_channel
    @queue = @channel.queue(topic, :durable => true)
  # @topic = @channel.topic(topic)


  # ObjectSpace.define_finalizer( self, self.class.finalize(@connection) )
    # @connection = Bunny.new("amqp://guest:guest@#{ENV['mq_url']}")
    # @connection.start
    # @channel = @connection.create_channel
    # @topic = @channel.topic(topic)
    # ObjectSpace.define_finalizer( self, self.class.finalize(@connection) )
  end

  def send (message)
    @queue.publish(message,:persistent => true)
    puts "Message #{message} sent"
  end

  def wait_to_receive(key,callback)
    @threads = []

    @threads << Thread.new {
      @queue = @channel.queue("compensation_queue",:durable => true)

      @channel.prefetch(1)
      begin
        @queue.subscribe(:ack => true, :block => true) do |delivery_info, properties, body|
           puts " [x] Received '#{body}'"
           # imitate some work
           callback.process(body)
           @channel.ack(delivery_info.delivery_tag)
         end
      rescue Interrupt => _
        @channel.close
        @connection.close
      end
    }
    @threads.each { |thr| thr.join }
  end

end
