# docker run -d -p 37017:27017 -v /Users/jomis/Documents/src/university/prototypes/smartdata/sad-framework/evaluation/data-service/dump:/dump --name mongodb1 mongo:3.2

# docker exec mongodb1 mongorestore /dump
#
# docker run -d -p 3801:8080 -v $PWD/restheart_1.yml:/opt/restheart/etc/restheart.yml:ro --name dataService1 --link mongodb1 softinstigate/restheart:2.0.1
docker run --name sad-redis -p 6379:6379 -d redis



docker run -d --hostname sad-rabbit --name sad-rabbit -p 8001:15672 -p 5671:5671 -p 5672:5672 rabbitmq:3-management

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-redis

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-rabbit

docker run -p 9000:8000 -e mq_url=172.17.0.3 -e master_hostname=$(hostname) -e redis_url=172.17.0.4 -d --name sad-proxy -t eu.gcr.io/sadframework/sad-proxy

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' sad-proxy

docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' dataService1

docker run -d -e http_proxy=http://172.17.0.4:8000 -e das_url=http://172.17.0.2:8080 -e client_name=urbem-client-1 -e client_type=fat -e client_kind=remote -e master_hostname=$(hostname) --name urbem-client-1 -t eu.gcr.io/sadframework/urbem-client-sim


docker run -d -e http_proxy=http://172.17.0.5:8000 -e das_url=http://172.17.0.2:8080 -e client_name=urbem-client-2 -e client_type=fat -e client_kind=remote -e master_hostname=$(hostname) --name urbem-client-2 -t eu.gcr.io/sadframework/urbem-client-sim
