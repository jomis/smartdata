docker run -p 3801:8080 -e data_service_port=8080 -d --name dataService1 eu.gcr.io/sadframework/data-service

# Copy data to host
docker cp dataService1:/usr/src/app/data/json/buildings.json ./migration/data/
docker cp dataService1:/usr/src/app/data/json/buildingblocks.json ./migration/data/
docker cp dataService1:/usr/src/app/data/json/mobility.json ./migration/data/
docker cp dataService1:/usr/src/app/data/json/network.json ./migration/data/
