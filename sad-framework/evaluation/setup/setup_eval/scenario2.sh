#!/bin/bash

ruby setup_eval.rb cleanInstances ALL

# ruby setup_eval.rb update ALL

ruby setup_eval.rb populateDataSim 10 ALL

ruby setup_eval.rb populateSAD ALL

ruby setup_eval.rb populateClient 5 vienna hongkong

ruby setup_eval.rb populateClient 5 berkeley vienna

ruby setup_eval.rb populateClient 5 hongkong berkeley 
