require 'rubygems'

require 'dogapi'

require 'yaml'

require 'csv'
require 'json'

set = "Scenario_1"

secrets = YAML.load_file('secrets.yml')

api_key = secrets["datadog_api_key"]
application_key = secrets["datadog_application_key"]

dog = Dogapi::Client.new(api_key, application_key)

# Get points from the last hour
timestamp = Time.now
command = "mkdir ./#{set}"
puts command
system(command)

#SCeanrio 2
# from = Time.now - 7200
# to = Time.now

# from = Time.new(2017,01,04, 21,21,0, "+01:00")
# to = Time.new(2017,01,04, 21,31,0, "+01:00")
#
# now = DateTime.now
# cleanTimeStampFrom = DateTime.new(now.year, now.month, now.day, 21, 21, 0, now.zone)


#SCeanrio 1
# from = Time.new(2017,01,04, 16,12,0, "+01:00")
# to = Time.new(2017,01,04, 16,22,0, "+01:00")
#
# now = DateTime.now
# cleanTimeStampFrom = DateTime.new(now.year, now.month, now.day, 16, 12, 0, now.zone)

from = Time.new(2017,01,04, 22,58,0, "+01:00")
to = Time.new(2017,01,04, 23,8,0, "+01:00")

now = DateTime.now
cleanTimeStampFrom = DateTime.new(now.year, now.month, now.day, 22, 58, 0, now.zone)

queries = []
queries << 'migrator.docker.migrationtime{*}'
queries << 'client.fat.remote.request.time.buildings{*}'
queries << 'client.fat.remote.request.time.networks{*}'
queries << 'client.fat.remote.request.time.mobility{*}'
queries << 'client.fat.remote.request.time.blocks{*}'
queries << 'system.net.bytes_sent{*}by{host}'
queries << 'system.net.bytes_rcvd{*}by{host}'

#DateTime.strptime("1318996912",'%Q')
#strftime("%H:%M:%SS")


queries.each do |query|
  puts query

  response = dog.get_points(query, from, to)
  if response != nil
    dog_values = response.last

    if[dog_values["series"]!= nil]

      series = dog_values["series"].first
      if(series != nil && series.has_key?("pointlist"))
        data_points = dog_values["series"].first["pointlist"]

        CSV.open("./#{set}/#{set}_#{query.split("{").first}.csv", "wb") do |csv|
          csv << ["time", "value"]
          for point in data_points
            # puts point.first
            conversion = DateTime.strptime("#{point.first}",'%Q')
            diff = (conversion - cleanTimeStampFrom)
            value = ((diff)* 24 * 60 * 60).to_i
            #DateTime.strptime("#{point.first}",'%Q').strftime("%H:%M:%S")
            csv << [value,point.last]
          end
        end
      else
        puts "No values for #{query}"
      end
    end
  end

end


# Splitted queries
queries = []
queries << 'system.net.bytes_sent{*}by{host}'
queries << 'system.net.bytes_rcvd{*}by{host}'
# queries << 'migrator.docker.migrationtime{*}by{host}'
queries << 'client.fat.remote.request.time.buildings{*}by{host}'
queries << 'client.fat.remote.request.time.networks{*}by{host}'
queries << 'client.fat.remote.request.time.mobility{*}by{host}'
queries << 'client.fat.remote.request.time.blocks{*}by{host}'

queries.each do |query|
  puts query

  response = dog.get_points(query, from, to)
  if response != nil
    dog_values = response.last

    if[dog_values["series"]!= nil]

      dog_values["series"].each do |series|
        scope = series["scope"]
        if(series != nil && series.has_key?("pointlist"))
          data_points = series["pointlist"]

          CSV.open("./#{set}/#{set}_#{query.split("{").first}_#{scope}.csv", "wb") do |csv|
            csv << ["time", "value"]
            for point in data_points
              conversion = DateTime.strptime("#{point.first}",'%Q')
              diff = (conversion - cleanTimeStampFrom)
              value = ((diff)* 24 * 60 * 60).to_i
              #DateTime.strptime("#{point.first}",'%Q').strftime("%H:%M:%S")
              csv << [value,point.last]
            end
          end
        else
          puts "No values for #{query}"
        end
      end
    end
  end

end



#Time markers

#2016-12-26 01:25:16 +0100
#Time.new(2008,6,21, 13,30,0, "+09:00")


# Eval 25 BAseline
# 2016-12-26 21:21:09 +0100
# Start sad
# 2016-12-26 20:46:17 +0100
