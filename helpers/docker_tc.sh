#!/bin/bash

sudo tc qdisc add dev ens3 root handle 1: prio
sudo tc qdisc add dev ens3 parent 1:3 handle 30: tbf rate 20kbit buffer 1600 limit  3000
sudo tc qdisc add dev ens3 parent 30:1 handle 31: netem  delay 200ms 100ms distribution normal
 
sudo tc filter add dev ens3 protocol ip parent 1:0 prio 3 u32 match ip dst 8.8.4.4/32 match ip src 172.17.0.3/32 flowid 1:3

# Filtering host adapter via class and destination ip
sudo tc qdisc del dev ens3 root
sudo tc qdisc add dev ens3 handle 1: root htb
sudo tc class add dev ens3 parent 1: classid 1:1 htb rate 1000Mbps
sudo tc class add dev ens3 parent 1: classid 1:11 htb rate 100Mbps
sudo tc qdisc add dev ens3 parent 1:11 handle 10: netem delay 200ms

sudo tc filter add dev ens3 protocol ip prio 1 u32 match ip dst 8.8.4.4/32 flowid 1:11



sudo iptables -A PREROUTING -t mangle -s 172.17.0.3/32 -i docker0 -j MARK --set-mark 6
sudo tc filter add dev ens3 protocol ip prio 1 handle 6 fw classid 1:11
