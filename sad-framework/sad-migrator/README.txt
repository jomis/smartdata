docker run -p 9293:9293 -e migration_manager_port=9293 -d --name sad-migrator eu.gcr.io/sadframework/sad-migrator

# Sample request

http://localhost:9292/migrate?strategy=mongodb&destination_host=localhost&destination_database=test&source_host=128.130.172.194&source_database=urbem&collection=districts
